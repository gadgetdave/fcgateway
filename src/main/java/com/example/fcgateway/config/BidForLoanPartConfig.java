package com.example.fcgateway.config;

import com.example.fcgateway.service.FundingCircleCookieProvider;
import com.example.fcgateway.usecase.bidforloanpart.BidForLoanPartUseCase;
import com.example.fcgateway.usecase.bidforloanpart.repository.BidForLoanPartRepository;
import com.example.fcgateway.repository.BidForLoanPartRepositoryOkHttp;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class BidForLoanPartConfig {
    @Value("${fcgateway.httpclient.baseurl.scheme}")
    private String baseUrlScheme;

    @Value("${fcgateway.httpclient.baseurl.host}")
    private String baseUrlHost;

    @Value("${fcgateway.httpclient.baseurl.port}")
    private int baseUrlPort;

    @Bean
    public FundingCircleCookieProvider fundingCircleCookieProvider() {
        return new FundingCircleCookieProvider();
    }

    @Autowired
    private FundingCircleCookieProvider fundingCircleCookieProvider;

    @Bean
    public BidForLoanPartUseCase bidForLoanPartUseCase() {
        return new BidForLoanPartUseCase(bidForLoanPartRepository());
    }

    @Bean
    public BidForLoanPartRepository bidForLoanPartRepository() {
        return new BidForLoanPartRepositoryOkHttp(
                new OkHttpClient()
                    .newBuilder()
                    .followRedirects(false)
                    .followSslRedirects(false)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            final Request original = chain.request();

                            final Request authorized = original.newBuilder()
                                    .addHeader("Cookie", fundingCircleCookieProvider.provideCookieHeaderString())
                                    .header("X-CSRF-Token", "56R+Z4J1jDSWQFSnu+f+eWKg4+Zsf/yfB2AB3CHSpH4qmXiPz3iwU4XsjdoPdDA045ZEf/4fXkcHOivOV5KvSQ==")
                                    .build();

                            return chain.proceed(authorized);
                        }
                    })
                    .build(),
                new HttpUrl.Builder()
                    .scheme(baseUrlScheme)
                    .host(baseUrlHost)
                    .port(baseUrlPort)
                    .build()
        );
    }
}
