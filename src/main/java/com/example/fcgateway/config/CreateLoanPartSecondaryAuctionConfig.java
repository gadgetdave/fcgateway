package com.example.fcgateway.config;

import com.example.fcgateway.repository.CreateLoanPartSecondaryAuctionRepositoryOkHttp;
import com.example.fcgateway.service.FundingCircleCookieProvider;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.CreateLoanPartSecondaryAuction;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.repository.CreateLoanPartSecondaryAuctionRepository;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class CreateLoanPartSecondaryAuctionConfig {
    @Value("${fcgateway.httpclient.baseurl.scheme}")
    private String baseUrlScheme;

    @Value("${fcgateway.httpclient.baseurl.host}")
    private String baseUrlHost;

    @Value("${fcgateway.httpclient.baseurl.port}")
    private int baseUrlPort;

    @Autowired
    private FundingCircleCookieProvider fundingCircleCookieProvider;

    @Bean
    public CreateLoanPartSecondaryAuction createLoanPartSecondaryAuctionUseCase() {
        return new CreateLoanPartSecondaryAuction(createLoanPartSecondaryAuctionRepository());
    }

    @Bean
    public CreateLoanPartSecondaryAuctionRepository createLoanPartSecondaryAuctionRepository() {
        return new CreateLoanPartSecondaryAuctionRepositoryOkHttp(
                new OkHttpClient()
                    .newBuilder()
                    .followRedirects(false)
                    .followSslRedirects(false)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            final Request original = chain.request();

                            final Request authorized = original.newBuilder()
                                    .addHeader("Cookie", fundingCircleCookieProvider.provideCookieHeaderString())
                                    .header("X-CSRF-Token", "UHXnBjPsUqQ4lPs2RxScj8ypRsH5Yi2+/MKrNQ9mV7XbFgYPESpP1SfbYdD3jsmmT+QGOmRa9JVimR8TSFO1oA==")
                                    .build();

                            return chain.proceed(authorized);
                        }
                    })
                    .build(),
                new HttpUrl.Builder()
                    .scheme(baseUrlScheme)
                    .host(baseUrlHost)
                    .port(baseUrlPort)
                    .build()
        );
    }
}
