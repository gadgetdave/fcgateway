package com.example.fcgateway.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

public class FundingCircleCookieProvider {

    private String cookieHeaderString;

    @Cacheable(value = "fc-auth-cookie")
    public String provideCookieHeaderString() {
        return cookieHeaderString;
    }

    @CacheEvict(value = "fc-auth-cookie", allEntries = true)
    public void updateCookieHeaderString(String cookieHeaderString) {
        this.cookieHeaderString = cookieHeaderString;
    }
}
