package com.example.fcgateway.repository;

import com.example.fcgateway.usecase.bidforloanpart.repository.BidForLoanPartRepository;
import com.example.fcgateway.usecase.model.Loan;
import com.example.fcgateway.usecase.model.LoanId;
import com.example.fcgateway.usecase.model.LoanPartBid;
import com.example.fcgateway.usecase.model.LoanPartBidId;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.Optional;

public class BidForLoanPartRepositoryOkHttp implements BidForLoanPartRepository {

    private OkHttpClient okHttpClient;
    private HttpUrl baseUrl;

    public BidForLoanPartRepositoryOkHttp(OkHttpClient okHttpClient, HttpUrl baseUrl) {
        this.okHttpClient = okHttpClient;
        this.baseUrl = baseUrl;
    }

    @Override
    public LoanPartBid createLoanPartBid(LoanPartBid loanPartBid) {

        RequestBody requestBody = new FormBody.Builder()
                .add("bid[annualised_rate]", String.format("%1$s", loanPartBid.getRate()))
                .add("bid[amount]", String.format("%1$s", loanPartBid.getAmount()))
                .build();

        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36")
                .url(baseUrl + String.format("loans/%1$s/bids", loanPartBid.getLoanId().getId()))
                .post(requestBody)
                .build();

        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();

            if (200 == response.code()) {
                ObjectMapper objectMapper = new ObjectMapper();

                JsonNode jsonNode = objectMapper.readTree(Optional.ofNullable(response.body()).orElseThrow(() -> new RuntimeException("No body returned.")).string());
                JsonNode bidNode = jsonNode.get("bid");

                return new LoanPartBid.Builder()
                        .withId(new LoanPartBidId(bidNode.get("bid_id").asInt()))
                        .withLoanId(loanPartBid.getLoanId())
                        .withAmount(loanPartBid.getAmount())
                        .withRate(loanPartBid.getRate())
                        .build();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            assert response != null;
            throw new RuntimeException(String.format("%1$s", Optional.ofNullable(response.body()).orElseThrow(() -> new RuntimeException("No body returned.")).string()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new RuntimeException();
    }

    @Override
    public Optional<Loan> findLoanById(LoanId loanId) {
        Request request = new Request.Builder()
                .header("Accept", "text/html")
                .url(baseUrl + String.format("loans/%1$s/auction", loanId.getId()))
                .build();

        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();

            if (200 == response.code()) {
                return Optional.of(new Loan(loanId));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }
}
