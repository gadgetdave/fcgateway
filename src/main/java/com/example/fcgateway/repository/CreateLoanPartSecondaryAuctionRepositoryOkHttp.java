package com.example.fcgateway.repository;

import com.example.fcgateway.usecase.createloanpartsecondaryauction.repository.CreateLoanPartSecondaryAuctionRepository;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuction;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuctionId;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import okhttp3.*;

import java.io.IOException;
import java.util.Optional;

public class CreateLoanPartSecondaryAuctionRepositoryOkHttp implements CreateLoanPartSecondaryAuctionRepository {
    private OkHttpClient okHttpClient;
    private HttpUrl baseUrl;

    public CreateLoanPartSecondaryAuctionRepositoryOkHttp(OkHttpClient okHttpClient, HttpUrl baseUrl) {
        this.okHttpClient = okHttpClient;
        this.baseUrl = baseUrl;
    }

    @Override
    public LoanPartSecondaryAuction createLoanPartSecondaryAuction(LoanPartSecondaryAuction loanPartSecondaryAuction) {

        JsonObject salesObject = new JsonObject();
        salesObject.add("loan_part_id", new JsonPrimitive(loanPartSecondaryAuction.getLoanPartId().toInt()));
        salesObject.add("markup", new JsonPrimitive(String.format("%1$s", loanPartSecondaryAuction.getMarkup().toDouble())));
        JsonArray salesArray = new JsonArray(1);
        salesArray.add(salesObject);

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("sales", salesArray);

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36")
                .url(baseUrl + "sell_individual_loan_parts/sell.json")
                .post(RequestBody.create(JSON, jsonObject.toString()))
                .build();

        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();

            if (201 == response.code()) {
                ObjectMapper objectMapper = new ObjectMapper();

                JsonNode jsonNode = objectMapper.readTree(Optional.ofNullable(response.body()).orElseThrow(() -> new RuntimeException("No body returned.")).string());
                JsonNode secondaryAuctionsNode = jsonNode.get("secondary_auctions");

                return new LoanPartSecondaryAuction.Builder()
                        .withId(new LoanPartSecondaryAuctionId(secondaryAuctionsNode.get(0).get("id").asInt()))
                        .withLoanPartId(loanPartSecondaryAuction.getLoanPartId())
                        .withMarkup(loanPartSecondaryAuction.getMarkup())
                        .build();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            assert response != null;
            throw new RuntimeException(String.format("%1$s", Optional.ofNullable(response.body()).orElseThrow(() -> new RuntimeException("No body returned.")).string()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new RuntimeException();
    }
}
