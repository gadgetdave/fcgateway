package com.example.fcgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@ImportResource("fcgateway-integration.xml")
@EnableIntegration
@EnableCaching
public class FundingCircleGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(FundingCircleGatewayApplication.class, args);
    }
}
