package com.example.fcgateway.usecase.createloanpartsecondaryauction.contract;

public class CreateLoanPartSecondaryResponse {
    public int id;
    public int loanPartId;
    public double markup;
}
