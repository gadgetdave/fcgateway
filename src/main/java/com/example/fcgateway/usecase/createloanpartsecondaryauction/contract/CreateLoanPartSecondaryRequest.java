package com.example.fcgateway.usecase.createloanpartsecondaryauction.contract;

public class CreateLoanPartSecondaryRequest {
    public int loanPartId;
    public double markup;
}
