package com.example.fcgateway.usecase.createloanpartsecondaryauction;

import com.example.fcgateway.usecase.createloanpartsecondaryauction.contract.CreateLoanPartSecondaryRequest;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.contract.CreateLoanPartSecondaryResponse;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.exception.NegativeMarkupException;
import com.example.fcgateway.usecase.model.LoanPartId;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuction;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuctionMarkup;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.repository.CreateLoanPartSecondaryAuctionRepository;

import java.util.Optional;

public class CreateLoanPartSecondaryAuction {

    private final CreateLoanPartSecondaryAuctionRepository repository;

    public CreateLoanPartSecondaryAuction(CreateLoanPartSecondaryAuctionRepository repository) {
        this.repository = repository;
    }

    public void handle(
            CreateLoanPartSecondaryRequest createLoanPartSecondaryRequest,
            CreateLoanPartSecondaryResponse createLoanPartSecondaryResponse) throws NegativeMarkupException {

        LoanPartSecondaryAuction loanPartSecondaryAuction = repository
                .createLoanPartSecondaryAuction(
                        new LoanPartSecondaryAuction.Builder()
                                .withLoanPartId(new LoanPartId(createLoanPartSecondaryRequest.loanPartId))
                                .withMarkup(new LoanPartSecondaryAuctionMarkup(createLoanPartSecondaryRequest.markup))
                                .build()
                );

        createLoanPartSecondaryResponse.id = Optional.ofNullable(loanPartSecondaryAuction.getId())
                .orElseThrow(() -> new RuntimeException("Missing LoanPartAuctionId"))
                .toInt();
        createLoanPartSecondaryResponse.loanPartId = Optional.ofNullable(loanPartSecondaryAuction.getLoanPartId())
                .orElseThrow(() -> new RuntimeException("Missing LoanPartId"))
                .toInt();
        createLoanPartSecondaryResponse.markup = Optional.ofNullable(loanPartSecondaryAuction.getMarkup())
                .orElseThrow(() -> new RuntimeException("Missing Markup"))
                .toDouble();
    }
}
