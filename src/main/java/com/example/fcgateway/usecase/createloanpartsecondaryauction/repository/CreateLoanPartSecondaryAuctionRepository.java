package com.example.fcgateway.usecase.createloanpartsecondaryauction.repository;

import com.example.fcgateway.usecase.model.LoanPartSecondaryAuction;

public interface CreateLoanPartSecondaryAuctionRepository {
    LoanPartSecondaryAuction createLoanPartSecondaryAuction(LoanPartSecondaryAuction loanPartSecondaryAuction);
}
