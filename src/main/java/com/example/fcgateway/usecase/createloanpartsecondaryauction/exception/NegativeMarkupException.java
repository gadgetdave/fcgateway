package com.example.fcgateway.usecase.createloanpartsecondaryauction.exception;

import com.example.fcgateway.usecase.exception.FcGatewayException;

public class NegativeMarkupException extends Exception implements FcGatewayException {
    public NegativeMarkupException(String message) {
        super(message);
    }
}
