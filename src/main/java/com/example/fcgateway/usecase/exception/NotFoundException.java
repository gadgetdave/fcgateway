package com.example.fcgateway.usecase.exception;

public class NotFoundException extends Exception implements FcGatewayException {
    public NotFoundException(String message) {
        super(message);
    }
}
