package com.example.fcgateway.usecase.bidforloanpart.repository;

import com.example.fcgateway.usecase.model.Loan;
import com.example.fcgateway.usecase.model.LoanId;
import com.example.fcgateway.usecase.model.LoanPartBid;

import java.util.Optional;

public interface BidForLoanPartRepository {
    LoanPartBid createLoanPartBid(LoanPartBid loanPartBid);

    Optional<Loan> findLoanById(LoanId loanId);
}
