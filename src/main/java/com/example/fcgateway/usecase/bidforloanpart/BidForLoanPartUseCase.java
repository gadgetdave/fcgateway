package com.example.fcgateway.usecase.bidforloanpart;

import com.example.fcgateway.usecase.bidforloanpart.contract.BidForLoanPartRequest;
import com.example.fcgateway.usecase.bidforloanpart.contract.BidForLoanPartResponse;
import com.example.fcgateway.usecase.exception.NotFoundException;
import com.example.fcgateway.usecase.model.LoanId;
import com.example.fcgateway.usecase.model.LoanPartBid;
import com.example.fcgateway.usecase.bidforloanpart.repository.BidForLoanPartRepository;

public class BidForLoanPartUseCase {
    private final BidForLoanPartRepository repository;

    public BidForLoanPartUseCase(BidForLoanPartRepository repository) {
        this.repository = repository;
    }

    public void handle(BidForLoanPartRequest bidForLoanPartRequest, BidForLoanPartResponse bidForLoanPartResponse) throws NotFoundException {
        LoanPartBid loanPartBid = repository.createLoanPartBid(
                    new LoanPartBid.Builder()
                            .withLoanId(repository.findLoanById(new LoanId(bidForLoanPartRequest.loanId))
                                    .orElseThrow(() -> new NotFoundException(""))
                                    .getLoanId()
                            )
                            .withRate(bidForLoanPartRequest.rate)
                            .withAmount(bidForLoanPartRequest.amount)
                            .build()
                    );

        bidForLoanPartResponse.id = loanPartBid.getId().getId();
        bidForLoanPartResponse.loanId = loanPartBid.getLoanId().getId();
        bidForLoanPartResponse.amount = loanPartBid.getAmount();
        bidForLoanPartResponse.rate = loanPartBid.getRate();
    }
}
