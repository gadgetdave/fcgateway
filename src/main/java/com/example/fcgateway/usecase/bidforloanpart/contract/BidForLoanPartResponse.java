package com.example.fcgateway.usecase.bidforloanpart.contract;

public class BidForLoanPartResponse {
    public int id;
    public int loanId;
    public int amount;
    public double rate;
}
