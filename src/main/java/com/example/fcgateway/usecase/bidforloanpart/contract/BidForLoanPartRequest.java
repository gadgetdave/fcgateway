package com.example.fcgateway.usecase.bidforloanpart.contract;

public class BidForLoanPartRequest {
    public int loanId;
    public int amount;
    public double rate;
}
