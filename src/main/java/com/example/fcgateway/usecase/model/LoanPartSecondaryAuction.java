package com.example.fcgateway.usecase.model;

public class LoanPartSecondaryAuction {
    private LoanPartSecondaryAuctionId id;

    private LoanPartId loanPartId;

    private LoanPartSecondaryAuctionMarkup markup;

    public LoanPartSecondaryAuctionId getId() {
        return id;
    }

    public LoanPartId getLoanPartId() {
        return loanPartId;
    }

    public LoanPartSecondaryAuctionMarkup getMarkup() {
        return markup;
    }

    public static class Builder {
        private LoanPartSecondaryAuction loanPartSecondaryAuction = new LoanPartSecondaryAuction();

        public Builder withId(LoanPartSecondaryAuctionId id) {
            loanPartSecondaryAuction.id = id;

            return this;
        }

        public Builder withLoanPartId(LoanPartId loanPartId) {
            loanPartSecondaryAuction.loanPartId = loanPartId;

            return this;
        }

        public LoanPartSecondaryAuction.Builder withMarkup(LoanPartSecondaryAuctionMarkup markup) {
            loanPartSecondaryAuction.markup = markup;

            return this;
        }

        public LoanPartSecondaryAuction build() {
            return loanPartSecondaryAuction;
        }
    }
}
