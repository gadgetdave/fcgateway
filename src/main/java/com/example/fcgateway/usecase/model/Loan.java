package com.example.fcgateway.usecase.model;

public class Loan {
    private LoanId loanId;

    public Loan(LoanId loanId) {
        this.loanId = loanId;
    }

    public LoanId getLoanId() {
        return loanId;
    }
}
