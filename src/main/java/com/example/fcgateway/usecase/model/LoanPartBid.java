package com.example.fcgateway.usecase.model;

public class LoanPartBid {
    private LoanPartBidId id;
    private LoanId loanId;
    private int amount;
    private double rate;

    public LoanPartBidId getId() {
        return id;
    }

    public LoanId getLoanId() {
        return loanId;
    }

    public int getAmount() {
        return amount;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoanPartBid that = (LoanPartBid) o;

        if (amount != that.amount) return false;
        if (Double.compare(that.rate, rate) != 0) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return loanId.equals(that.loanId);
    }

    public static class Builder {
        private LoanPartBid loanPartBid = new LoanPartBid();

        public Builder withId(LoanPartBidId id) {
            loanPartBid.id = id;

            return this;
        }

        public Builder withLoanId(LoanId loanId) {
            loanPartBid.loanId = loanId;

            return this;
        }

        public Builder withAmount(int amount) {
            loanPartBid.amount = amount;

            return this;
        }

        public Builder withRate(double rate) {
            loanPartBid.rate = rate;

            return this;
        }

        public LoanPartBid build() {
            return loanPartBid;
        }
    }
}
