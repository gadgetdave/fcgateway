package com.example.fcgateway.usecase.model;

public class LoanId {
    private int id;

    public LoanId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoanId loanId = (LoanId) o;

        return id == loanId.id;
    }
}
