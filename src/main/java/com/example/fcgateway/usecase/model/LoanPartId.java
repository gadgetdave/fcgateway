package com.example.fcgateway.usecase.model;

public class LoanPartId {
    private final int id;

    public LoanPartId(int id) {
        this.id = id;
    }

    public int toInt() {
        return id;
    }
}
