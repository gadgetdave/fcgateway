package com.example.fcgateway.usecase.model;

public class LoanPartSecondaryAuctionId {
    private int id;

    public LoanPartSecondaryAuctionId(int id) {
        this.id = id;
    }

    public int toInt() {
        return id;
    }
}
