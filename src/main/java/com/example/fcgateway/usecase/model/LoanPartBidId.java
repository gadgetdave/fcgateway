package com.example.fcgateway.usecase.model;

public class LoanPartBidId {
    private int id;

    public LoanPartBidId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoanPartBidId that = (LoanPartBidId) o;

        return id == that.id;
    }
}
