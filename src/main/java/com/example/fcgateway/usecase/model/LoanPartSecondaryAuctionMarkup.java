package com.example.fcgateway.usecase.model;

import com.example.fcgateway.usecase.createloanpartsecondaryauction.exception.NegativeMarkupException;

public class LoanPartSecondaryAuctionMarkup {
    private final double markup;

    public LoanPartSecondaryAuctionMarkup(double markup) throws NegativeMarkupException {
        assertPositiveMarkup(markup);

        this.markup = markup;
    }

    public double toDouble() {
        return markup;
    }

    private void assertPositiveMarkup(double markup) throws NegativeMarkupException {
        if (markup < 0) {
            throw new NegativeMarkupException(String.format("Expected positive double for markup %1$s given.", markup));
        }
    }
}
