package com.example.fcgateway.controller;

import com.example.fcgateway.contract.ApiResponse;
import com.example.fcgateway.exception.HttpNotFoundException;
import com.example.fcgateway.service.FundingCircleCookieProvider;
import com.example.fcgateway.usecase.bidforloanpart.BidForLoanPartUseCase;
import com.example.fcgateway.usecase.bidforloanpart.contract.BidForLoanPartRequest;
import com.example.fcgateway.usecase.bidforloanpart.contract.BidForLoanPartResponse;
import com.example.fcgateway.usecase.exception.NotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.util.Map;

@RestController
public class BidForLoanPartController {

    @Autowired
    private BidForLoanPartUseCase bidForLoanPartUseCase;

    @Autowired
    private FundingCircleCookieProvider fundingCircleCookieProvider;

    @RequestMapping(value = "/loan/{loanId}/part/bid", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> bidForLoanPart(@PathVariable int loanId, @RequestBody Map content) throws JsonProcessingException, MalformedURLException, HttpNotFoundException {

        fundingCircleCookieProvider.updateCookieHeaderString("");

        BidForLoanPartRequest bidForLoanPartRequest = new BidForLoanPartRequest();
        bidForLoanPartRequest.loanId = loanId;
        bidForLoanPartRequest.amount = ((int) content.get("amount"));
        bidForLoanPartRequest.rate = ((double) content.get("rate"));

        BidForLoanPartResponse bidForLoanPartResponse = new BidForLoanPartResponse();

        try {
            bidForLoanPartUseCase.handle(bidForLoanPartRequest, bidForLoanPartResponse);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ApiResponse().send(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return new ApiResponse(bidForLoanPartResponse).send(HttpStatus.CREATED);
    }
}
