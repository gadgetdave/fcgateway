package com.example.fcgateway.controller;

import com.example.fcgateway.contract.ApiResponse;
import com.example.fcgateway.exception.HttpNotFoundException;
import com.example.fcgateway.service.FundingCircleCookieProvider;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.CreateLoanPartSecondaryAuction;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.contract.CreateLoanPartSecondaryRequest;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.contract.CreateLoanPartSecondaryResponse;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.exception.NegativeMarkupException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.util.Map;

@RestController
public class CreateSecondaryLoanPartAuctionController {

    @Autowired
    private CreateLoanPartSecondaryAuction createLoanPartSecondaryAuction;

    @Autowired
    private FundingCircleCookieProvider fundingCircleCookieProvider;

    @RequestMapping(value = "/loan/{loanId}/part/{loanPartId}/secondary-auction", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> createLoanPartSecondaryAuction(@PathVariable int loanId, @PathVariable int loanPartId, @RequestBody Map content) throws JsonProcessingException, MalformedURLException, HttpNotFoundException {

        fundingCircleCookieProvider.updateCookieHeaderString("optimizelyEndUserId=oeu1493636349964r0.45486968306839715; _msuuid_x4bomf7pw0=7E52F868-023F-4132-A597-EDD1A35B13E9; cookie_policy=1; __lc.visitor_id.8288151=S1495390460.e536a91607; __utmt=1; __storejs__=%22__storejs__%22; _pk_id.206-1.5a74=23f621faa04e5a33.1493636351.0.1499897144..; _fundingcircle.com_session=Q05WMkFhdUZUWXFVcVV0ekdoRmtrM0N3TE9kZWtpdUxJSjZlNzYyc0lrOVE1dTFlMHBxYm44d09JWHNIVjdjOU0wcDVIYXU5T3o0TitkOXpTU2EwdUJXL1dVSEVZSzJUYVNtaEhCWk5acEhPUzR4Z1dPN1lVUzNJcUprdCtsb21QdllOdXpwdWFjaFRsR01YT3J1dCtEaXRvakVoWTRzWmlrRGUxWmJmYzV2U3NLRlErckdVTVRCVHhEcmlqMExhVk5KR2kxNTI0OE1nY3hjM1hYR3ZwejUxSGFxTFNmbDNORitMQjJuWlhBdG40Z2J6akp4Y2t2TFZPYjFzcEdvNXJlVXo5L1ZFSUN2Rzc0V0c1aGF5VE9TdzcyQ2pyM0VtRjNtM3YvT2ZIUU4xZmZNdHFMTnl1akNPTWYxZWpBM2J0Y3laTXR4TnBnNk9ITXkxQ1NJUzBHNTdESEpvNi94Vjllc3NnRTRCWU4zZFNCajZhRmxoNktQeUV2enhUOTRJa0FjNXdXSVJlZjAwR0FRQmRzZmNITUN5RUE3L3Q0MWt4T1o0eE9paHRWSDdFdW5hbUxmZjl4SDQrN0lqYytSeS0tRm1CbGVSRUYza1ZUMVR5eERua1g1QT09--1ce65dc248d830c92029217c0d3438b5ecaf5a94; optimizelySegments=%7B%22315524136%22%3A%22referral%22%2C%22315524137%22%3A%22gc%22%2C%22315536086%22%3A%22false%22%7D; optimizelyBuckets=%7B%228151534799%22%3A%228157152592%22%2C%228329731615%22%3A%228330430968%22%2C%228405143834%22%3A%228409201959%22%2C%228462574832%22%3A%228463544971%22%7D; __utma=74436741.255768696.1493636350.1499892360.1499896664.32; __utmb=74436741.6.10.1499896664; __utmc=74436741; __utmz=74436741.1493636350.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.255768696.1493636350; _gid=GA1.2.45409416.1499710296; _uetsid=_uet7e67bfee; __CT_Data=gpv=203&apv_53342_www=168&cpv_53342_www=64&rpv_53342_www=23; WRUIDAWS=1273541002616961; _bizo_bzid=75c29cd2-1f87-424e-a825-7d24fae8f80c; _bizo_cksm=00FE4F270B739FD3; _bizo_np_stats=155%3D105%2C; XSRF-TOKEN=ttDDQm1UQQz2TZs8XVoktVFy9lP8kc2IRSOxdE1uxc89syJLT5JcfekCAdrtwHGc0j%2B2qGGpFKPbeAVSClsn2g%3D%3D; _fca_frontend_session=SXppeGhpT0p3WU1DV0VIT0NRN0pCRVNablpWSlVtaDNXZll4Y3JSQ01UTXlYQXNGVWZtV1VHZ1NjLzd4cEc5ZXh2TDBGRnZOelFyWHNRMFVYdjVVTFJSSGY3OUNyTFZYTVY3eGlRNG9zeVE4WmFxSG9oNURWTXduUHhoSVdoUys3a2ZnSHFudTdPcUZleEtqN25waHBCKzRHU0xVVUVBTmdabjVmK3hXWUhESHZYKzRzOVpTUVZGeVBveGswbHo1Z1FDSTQ1bTFpekZ2OHNLRVNWcUc2eDlWazYyMlRhOWVjUFJ6NmFGT0RpSlorR3dialRtSG1VYitTQlJPNDIvSlhRalU0Q29mUlhtQk1GOEVWRVI2R0FMeGRpRnZzbE5FVDlWUi85c3ViMTFtUUZONVhpZFd2TkhOSTZNelNwdVdVUnhsMEdUdkpXckdKem8vaU56YzlIVmFGZ1JvaVBMZGNJVjY4akJPSzJsSW4rVXRudlZwdU9OZjZHUE5PNWpSVlBDYUMyWFJHUjd5bVFmeEZHcS9jK3dvRTc3RDBvUmx5LzJNOEJKVUM2eHo2MmhyY3NMcytOdllKVDdUUmtFZHdxMWFSd1k4QjQyQ09WaDlXeCt0eTFzVElLY2syMHpnaWpuaVNUSHpZbXhLOVRNNUtwVktUYjd5a1h5NHJxdmgwMUcwWjBzU0orZlVLN2xZY1VxWkRneUJnSi9nRStyL0hjSEpodW9lcjliSGIrbG5TU2IvM1ZaZXgzR09EN0ZqM0xlMUtwL2FhWEtBMlBRNUo0aDQwUT09LS0rYW12WTJqNTUvQUEvM1lXdER0UU5RPT0%3D--5caa546ae68705f1a35d0cf3c6fa488c7655afac");

        CreateLoanPartSecondaryRequest createLoanPartSecondaryRequest = new CreateLoanPartSecondaryRequest();

        createLoanPartSecondaryRequest.loanPartId = loanPartId;
        createLoanPartSecondaryRequest.markup = ((double) content.get("markup"));

        CreateLoanPartSecondaryResponse createLoanPartSecondaryResponse = new CreateLoanPartSecondaryResponse();
        try {
            createLoanPartSecondaryAuction.handle(createLoanPartSecondaryRequest, createLoanPartSecondaryResponse);
        } catch (NegativeMarkupException e) {
            e.printStackTrace();
            return new ApiResponse().send(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return new ApiResponse(createLoanPartSecondaryResponse).send(HttpStatus.CREATED);
    }
}
