package com.example.fcgateway.repository;

import com.example.fcgateway.service.FundingCircleCookieProvider;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.exception.NegativeMarkupException;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.repository.CreateLoanPartSecondaryAuctionRepository;
import com.example.fcgateway.usecase.model.LoanPartId;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuction;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuctionMarkup;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.UrlPathPattern;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ComponentScan(value = "com.example.fcgateway")
@TestPropertySource(locations = "classpath:wiremock-test.properties")
public class CreateLoanPartSecondaryAuctionRepositoryOkHttpTest {

    private static final String AUTH_COOKIE_VALUE = "authorised";

    private static final String AUTH_COOKIE_KEY = "_fundingcircle.com_session";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule();

    @Autowired
    private CreateLoanPartSecondaryAuctionRepository createLoanPartSecondaryAuctionRepository;

    @Autowired
    private FundingCircleCookieProvider fundingCircleCookieProvider;

    @Before
    public void setUp() throws Exception {
        fundingCircleCookieProvider.updateCookieHeaderString(AUTH_COOKIE_KEY + "=" + AUTH_COOKIE_VALUE);
    }

    @Test
    public void createLoanPartSecondaryAuction() throws NegativeMarkupException {
        UrlPathPattern urlPattern = urlPathEqualTo("/sell_individual_loan_parts/sell.json");
        stubFor(post(urlPattern)
                .withHeader("Accept", containing("application/json"))
                .withRequestBody(equalTo(String.format("{\"sales\":[{\"loan_part_id\":123,\"markup\":\"0.3\"}]}")))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"secondary_auctions\":[{\"id\":23343696}]}")));

        assertEquals(
                23343696,
                createLoanPartSecondaryAuctionRepository
                        .createLoanPartSecondaryAuction(
                                new LoanPartSecondaryAuction.Builder()
                                        .withLoanPartId(new LoanPartId(123))
                                        .withMarkup(new LoanPartSecondaryAuctionMarkup(0.3))
                                        .build()
                        )
                        .getId().toInt()
        );
    }

    @Test
    public void createLoanPartSecondaryAuctionNoBody() {
        UrlPathPattern urlPattern = urlPathEqualTo("/sell_individual_loan_parts/sell.json");
        stubFor(post(urlPattern)
                .withHeader("Accept", containing("application/json"))
                .withRequestBody(equalTo(String.format("{\"sales\":[{\"loan_part_id\":123,\"markup\":\"0.3\"}]}")))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody("")));

        assertThrows(RuntimeException.class, () -> createLoanPartSecondaryAuctionRepository
                .createLoanPartSecondaryAuction(
                        new LoanPartSecondaryAuction.Builder()
                                .withLoanPartId(new LoanPartId(123))
                                .withMarkup(new LoanPartSecondaryAuctionMarkup(0.3))
                                .build()
                ));
    }
}
