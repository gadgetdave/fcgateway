package com.example.fcgateway.repository;

import com.example.fcgateway.service.FundingCircleCookieProvider;
import com.example.fcgateway.usecase.bidforloanpart.repository.BidForLoanPartRepository;
import com.example.fcgateway.usecase.exception.NotFoundException;
import com.example.fcgateway.usecase.model.LoanId;
import com.example.fcgateway.usecase.model.LoanPartBid;
import com.example.fcgateway.usecase.model.LoanPartBidId;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.UrlPathPattern;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ComponentScan(value = "com.example.fcgateway")
@TestPropertySource(locations = "classpath:wiremock-test.properties")
public class BidForLoanPartRepositoryOkHttpTest {

    private static final int LOAN_ID = 194322;

    private static final int LOAN_PART_BID_ID = 347834;

    private static final String AUTH_COOKIE_VALUE = "authorised";

    private static final String AUTH_COOKIE_KEY = "_fundingcircle.com_session";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule();

    @Autowired
    private BidForLoanPartRepository bidForLoanPartRepository;

    @Autowired
    private FundingCircleCookieProvider fundingCircleCookieProvider;

    @Before
    public void setUp() throws Exception {
        fundingCircleCookieProvider.updateCookieHeaderString(AUTH_COOKIE_KEY + "=" + AUTH_COOKIE_VALUE);
    }

    @Test
    public void loanExists() throws NotFoundException {
        UrlPathPattern urlPattern = urlPathEqualTo(String.format("/loans/%1$s/auction", LOAN_ID));
        stubFor(get(urlPattern)
                .withHeader("Accept", containing("text/html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBody("Some content")));

        assertEquals(LOAN_ID, bidForLoanPartRepository.findLoanById(new LoanId(LOAN_ID)).orElseThrow(() -> new NotFoundException("")).getLoanId().getId());
    }

    @Test
    public void verifyAuthorised() throws NotFoundException {
        UrlPathPattern urlPattern = urlPathEqualTo(String.format("/loans/%1$s/auction", LOAN_ID));
        stubFor(get(urlPattern)
                .withHeader("Accept", containing("text/html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBody("Some content")));

        bidForLoanPartRepository.findLoanById(new LoanId(LOAN_ID));

        verify(getRequestedFor(urlPattern).withCookie(AUTH_COOKIE_KEY, matching(AUTH_COOKIE_VALUE)));
    }

    @Test
    public void loanDoesNotExists() {
        stubFor(get(urlEqualTo(String.format("/loans/%1$s/auction", 0)))
                .withHeader("Accept", equalTo("text/html"))
                .willReturn(aResponse()
                        .withStatus(404)
                        .withHeader("Content-Type", "text/html")
                        .withBody("Some content")));


        assertFalse(bidForLoanPartRepository.findLoanById(new LoanId(0)).isPresent());
    }

    @Test
    public void createLoanPartBid() {
        UrlPathPattern urlPattern = urlPathEqualTo(String.format("/loans/%1$s/bids", LOAN_ID));

        String responseString = "{\"status\":\"ok\",\"bid\":{\"annualised_rate\":\"7.5\",\"amount\":\"20\",\"bid_id\":%1$s},\"message\":[[\"notice\",\"Your bid has been placed.\"]],\"stats\":{\"live_bids\":\"\\u0026pound;20.00\",\"average_rate\":\"7.5\",\"max_rate\":7.5,\"account_balance\":\"\\u0026pound;83.77\"}}";

        LoanPartBid loanPartBid = new LoanPartBid.Builder()
                .withLoanId(new LoanId(LOAN_ID))
                .withAmount(20)
                .withRate(7.5)
                .build();

        stubFor(post(urlPattern)
                .withHeader("Accept", containing("application/json"))
                .withRequestBody(containing(
                        "bid%5Bannualised_rate%5D=" + String.format("%1$s", loanPartBid.getRate())
                                + "&bid%5Bamount%5D=" + String.format("%1$s", loanPartBid.getAmount()))
                )
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(String.format(responseString, LOAN_PART_BID_ID))));


        assertEquals(
                new LoanPartBid.Builder()
                        .withId(new LoanPartBidId(LOAN_PART_BID_ID))
                        .withLoanId(loanPartBid.getLoanId())
                        .withAmount(loanPartBid.getAmount())
                        .withRate(loanPartBid.getRate())
                        .build(),
                bidForLoanPartRepository.createLoanPartBid(loanPartBid)
        );
    }

    @Test
    public void createLoanPartBidNoBody() {
        UrlPathPattern urlPattern = urlPathEqualTo(String.format("/loans/%1$s/bids", LOAN_ID));
        stubFor(post(urlPattern)
                .withHeader("Accept", containing("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));

        LoanPartBid loanPartBid = new LoanPartBid.Builder()
                .withLoanId(new LoanId(LOAN_ID))
                .withAmount(20)
                .build();

        assertThrows(RuntimeException.class, () -> bidForLoanPartRepository.createLoanPartBid(loanPartBid));
    }
}
