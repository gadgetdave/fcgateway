package com.example.fcgateway.usecase.createloanpartsecondaryauction;

import com.example.fcgateway.usecase.createloanpartsecondaryauction.contract.CreateLoanPartSecondaryRequest;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.contract.CreateLoanPartSecondaryResponse;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.exception.NegativeMarkupException;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuction;
import com.example.fcgateway.usecase.createloanpartsecondaryauction.repository.CreateLoanPartSecondaryAuctionRepositoryInMemory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateLoanPartSecondaryAuctionTest {

    private CreateLoanPartSecondaryAuction createLoanPartSecondaryAuction;
    private CreateLoanPartSecondaryRequest createLoanPartSecondaryRequest;
    private CreateLoanPartSecondaryResponse createLoanPartSecondaryResponse;
    private CreateLoanPartSecondaryAuctionRepositoryInMemory createLoanPartSecondaryAuctionRepository;

    @BeforeEach
    void setUp() {
        createLoanPartSecondaryAuctionRepository = new CreateLoanPartSecondaryAuctionRepositoryInMemory();
        createLoanPartSecondaryAuction = new CreateLoanPartSecondaryAuction(createLoanPartSecondaryAuctionRepository);
        createLoanPartSecondaryRequest = new CreateLoanPartSecondaryRequest();
        createLoanPartSecondaryRequest.loanPartId = 123;
        createLoanPartSecondaryRequest.markup = 0.3;
        createLoanPartSecondaryResponse = new CreateLoanPartSecondaryResponse();
    }

    @Test
    void returnsSecondaryAuctionData() throws NegativeMarkupException {
        createLoanPartSecondaryAuction.handle(createLoanPartSecondaryRequest, createLoanPartSecondaryResponse);

        LoanPartSecondaryAuction lastLoanPartSecondaryAuction = createLoanPartSecondaryAuctionRepository.getLastLoanPartSecondaryAuction();

        assertEquals(
                Optional.ofNullable(lastLoanPartSecondaryAuction.getId())
                        .orElseThrow(() -> new RuntimeException("Missing LoanPartSecondaryAuctionId"))
                        .toInt(),
                createLoanPartSecondaryResponse.id
        );
        assertEquals(createLoanPartSecondaryRequest.loanPartId, createLoanPartSecondaryResponse.loanPartId);
        assertEquals(createLoanPartSecondaryRequest.markup, createLoanPartSecondaryResponse.markup, 0);
    }

    @Test
    void negativeMarkupThrowsInvalidArgumentException() {
        createLoanPartSecondaryRequest.markup = -0.1;
        assertThrows(NegativeMarkupException.class, () -> createLoanPartSecondaryAuction.handle(createLoanPartSecondaryRequest, createLoanPartSecondaryResponse));
    }
}
