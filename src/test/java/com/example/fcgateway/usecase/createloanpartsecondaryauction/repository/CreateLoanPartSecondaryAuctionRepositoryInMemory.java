package com.example.fcgateway.usecase.createloanpartsecondaryauction.repository;

import com.example.fcgateway.usecase.model.LoanPartSecondaryAuction;
import com.example.fcgateway.usecase.model.LoanPartSecondaryAuctionId;

public class CreateLoanPartSecondaryAuctionRepositoryInMemory implements CreateLoanPartSecondaryAuctionRepository {

    private int autoIncrementLoanPartId = 1;

    private LoanPartSecondaryAuction lastLoanPartSecondaryAuction;

    @Override
    public LoanPartSecondaryAuction createLoanPartSecondaryAuction(LoanPartSecondaryAuction loanPartSecondaryAuction) {
        lastLoanPartSecondaryAuction = new LoanPartSecondaryAuction.Builder()
                .withId(new LoanPartSecondaryAuctionId(autoIncrementLoanPartId++))
                .withLoanPartId(loanPartSecondaryAuction.getLoanPartId())
                .withMarkup(loanPartSecondaryAuction.getMarkup())
                .build();

        return lastLoanPartSecondaryAuction;
    }

    public LoanPartSecondaryAuction getLastLoanPartSecondaryAuction() {
        return lastLoanPartSecondaryAuction;
    }
}
