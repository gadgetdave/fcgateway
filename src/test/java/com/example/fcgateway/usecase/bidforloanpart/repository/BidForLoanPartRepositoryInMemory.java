package com.example.fcgateway.usecase.bidforloanpart.repository;

import com.example.fcgateway.usecase.model.Loan;
import com.example.fcgateway.usecase.model.LoanId;
import com.example.fcgateway.usecase.model.LoanPartBid;
import com.example.fcgateway.usecase.model.LoanPartBidId;

import java.util.List;
import java.util.Optional;

public class BidForLoanPartRepositoryInMemory implements BidForLoanPartRepository {
    private LoanPartBid lastLoanPartBid;
    private int autoIncrementLoanPartId = 0;
    private List<Loan> loans;

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }

    public LoanPartBid getLastLoanPartBid() {
        return lastLoanPartBid;
    }

    @Override
    public LoanPartBid createLoanPartBid(LoanPartBid loanPartBid) {
        lastLoanPartBid = new LoanPartBid.Builder()
                .withId(new LoanPartBidId(autoIncrementLoanPartId++))
                .withAmount(loanPartBid.getAmount())
                .withLoanId(loanPartBid.getLoanId())
                .build();

        return getLastLoanPartBid();
    }

    @Override
    public Optional<Loan> findLoanById(LoanId loanId) {
        return loans.stream()
                .filter(l -> l.getLoanId().getId() == loanId.getId())
                .findFirst();
    }
}
