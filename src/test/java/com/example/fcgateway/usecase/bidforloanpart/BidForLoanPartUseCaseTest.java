package com.example.fcgateway.usecase.bidforloanpart;

import com.example.fcgateway.usecase.bidforloanpart.contract.BidForLoanPartRequest;
import com.example.fcgateway.usecase.bidforloanpart.contract.BidForLoanPartResponse;
import com.example.fcgateway.usecase.exception.NotFoundException;
import com.example.fcgateway.usecase.model.Loan;
import com.example.fcgateway.usecase.model.LoanId;
import com.example.fcgateway.usecase.model.LoanPartBid;
import com.example.fcgateway.usecase.model.LoanPartBidId;
import com.example.fcgateway.usecase.bidforloanpart.repository.BidForLoanPartRepositoryInMemory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BidForLoanPartUseCaseTest {

    private static final int LOAN_ID = 123;
    private BidForLoanPartUseCase bidForLoanPartUseCase;
    private BidForLoanPartRequest bidForLoanPartRequest;
    private BidForLoanPartResponse bidForLoanPartResponse;
    private BidForLoanPartRepositoryInMemory bidForLoanPartRepositoryInMemory;

    @BeforeEach
    void setUp() {
        bidForLoanPartRepositoryInMemory = new BidForLoanPartRepositoryInMemory();
        bidForLoanPartRepositoryInMemory.setLoans(Collections.singletonList(new Loan(new LoanId(LOAN_ID))));
        bidForLoanPartUseCase = new BidForLoanPartUseCase(bidForLoanPartRepositoryInMemory);
        bidForLoanPartRequest = new BidForLoanPartRequest();
        bidForLoanPartRequest.loanId = LOAN_ID;
        bidForLoanPartRequest.amount = 22;
        bidForLoanPartRequest.rate = 8.2;
        bidForLoanPartResponse = new BidForLoanPartResponse();
    }

    @Test
    void bidForLoanPartReturnsLoanPartBidValues() {
        try {
            bidForLoanPartUseCase.handle(bidForLoanPartRequest, bidForLoanPartResponse);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        assertEquals(bidForLoanPartRepositoryInMemory.getLastLoanPartBid().getId(), new LoanPartBidId(bidForLoanPartResponse.id));
        assertEquals(bidForLoanPartRepositoryInMemory.getLastLoanPartBid().getLoanId().getId(), bidForLoanPartResponse.loanId);
        assertEquals(bidForLoanPartRepositoryInMemory.getLastLoanPartBid().getAmount(), bidForLoanPartResponse.amount);
        assertEquals(bidForLoanPartRepositoryInMemory.getLastLoanPartBid().getRate(), bidForLoanPartResponse.rate);
    }

    @Test
    void bidForLoanPartCreatesLoanPartBid() {
        try {
            bidForLoanPartUseCase.handle(bidForLoanPartRequest, bidForLoanPartResponse);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        LoanPartBid expectedLoanPartBid = new LoanPartBid.Builder()
                .withId(new LoanPartBidId(bidForLoanPartResponse.id))
                .withLoanId(new LoanId(bidForLoanPartRequest.loanId))
                .withAmount(bidForLoanPartRequest.amount)
                .build();

        assertEquals(expectedLoanPartBid, bidForLoanPartRepositoryInMemory.getLastLoanPartBid());
    }

    @Test
    void bidForLoanPartNotFoundException() {
        bidForLoanPartRequest.loanId = 0;

        assertThrows(NotFoundException.class, () -> bidForLoanPartUseCase.handle(bidForLoanPartRequest, bidForLoanPartResponse));
    }
}
